import 'package:flutter/material.dart';

void main() {
  runApp(MaterialApp(
    home: JLCard(),
  ));
}

class JLCard extends StatefulWidget {
  @override
  _JLCardState createState() => _JLCardState();
}

class _JLCardState extends State<JLCard> {
  bool isActive = false;
  String status = 'offline';
  IconData statusIcon = Icons.star_border_outlined;
  @override
  Widget build(BuildContext context) {
    if (isActive) {
      status = 'Online';
      statusIcon = Icons.star;
    } else {
      status = 'Offline';
      statusIcon = Icons.star_border_outlined;
    }
    return Scaffold(
      backgroundColor: Colors.grey[300],
      appBar: AppBar(
        title: Text(
          'Justice League ID Card',
        ),
        backgroundColor: Colors.teal,
        elevation: 0.0,
      ),
      body: Padding(
        padding: EdgeInsets.fromLTRB(30.0, 40.0, 30.0, 0.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Center(
              child: CircleAvatar(
                backgroundImage: AssetImage('assets/images/batman.jpg'),
                radius: 40.0,
              ),
            ),
            Divider(
              height: 60.0,
              color: Colors.grey[400],
            ),
            Text(
              'NAME',
              style: TextStyle(
                letterSpacing: 1.0,
                fontWeight: FontWeight.normal,
                color: Colors.grey[600],
                fontSize: 18.0,
              ),
            ),
            SizedBox(height: 10.0),
            Text(
              'Batman',
              style: TextStyle(
                letterSpacing: 2.0,
                fontWeight: FontWeight.bold,
                color: Colors.teal[600],
                fontSize: 28.0,
              ),
            ),
            SizedBox(height: 30.0),
            Text(
              'POSITION',
              style: TextStyle(
                letterSpacing: 1.0,
                fontWeight: FontWeight.normal,
                color: Colors.grey[600],
                fontSize: 18.0,
              ),
            ),
            SizedBox(height: 10.0),
            Text(
              'Detector',
              style: TextStyle(
                letterSpacing: 2.0,
                fontWeight: FontWeight.bold,
                color: Colors.teal[600],
                fontSize: 28.0,
              ),
            ),
            SizedBox(height: 30.0),
            Text(
              'STATUS',
              style: TextStyle(
                letterSpacing: 1.0,
                fontWeight: FontWeight.normal,
                color: Colors.grey[600],
                fontSize: 18.0,
              ),
            ),
            SizedBox(height: 10.0),
            Text(
              '$status',
              style: TextStyle(
                letterSpacing: 2.0,
                fontWeight: FontWeight.bold,
                color: Colors.teal[600],
                fontSize: 28.0,
              ),
            ),
            SizedBox(height: 30.0),
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Icon(
                  Icons.email,
                  color: Colors.grey[600],
                ),
                SizedBox(width: 10.0),
                Text(
                  'batman@batmail.com',
                  style: TextStyle(
                    letterSpacing: 0.5,
                    fontWeight: FontWeight.normal,
                    color: Colors.grey[600],
                    fontSize: 14.0,
                  ),
                )
              ],
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          setState(() {
            this.isActive = !this.isActive;
          });
        },
        child: Icon(
          statusIcon,
        ),
        backgroundColor: Colors.teal,
      ),
    );
  }
}

